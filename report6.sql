USE etlerdb;

DROP procedure IF EXISTS PROMOMonitoring;

DELIMITER $$

CREATE PROCEDURE PROMOMonitoring( IN nmgkUnit INT
                                , IN regionId INT
                                , IN cityId INT
                                , IN retailerId INT
                                , IN pointId INT
                                , IN firstDate DATE
                                , IN lastDate DATE
                                , OUT sequal VARCHAR(104096))
BEGIN
  DECLARE pivot_campaigns VARCHAR(93072);
  DECLARE whereClause VARCHAR(5255) DEFAULT 'WHERE ';
  DECLARE groupbyClause VARCHAR(50) DEFAULT 'GROUP BY taskId, pointId, skuId';

  IF (nmgkUnit IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, 'nmgkUnit = ', nmgkUnit, ' AND ');
  END IF;

  SET whereClause = CONCAT(whereClause, '(timestamp BETWEEN ''', firstDate, ''' AND ''', lastDate, ''' )');

  IF (regionId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND regionId = ', regionId);
  END IF;

  IF (cityId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND cityId = ', cityId);
  END IF;

  IF (retailerId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND retailerId = ', retailerId);
  END IF;

  IF (pointId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND pointId = ', pointId);
  END IF;

  SELECT GROUP_CONCAT(DISTINCT(CONCAT('GROUP_CONCAT(IF(tm.campaign = ''', campaign , ''', 1, NULL)) AS campaign_', campaign))) INTO pivot_campaigns FROM trademarketing WHERE (timestamp BETWEEN firstDate AND lastDate);

  SET sequal = CONCAT('SELECT userId, timestamp, taskId, nmgkUnit, regionId, cityId, pointId, nmgkTtType, retailerId, skuId, ', pivot_campaigns, ', MAX(price) AS price, MAX(priceBefore) AS priceBefore, MAX(discount) AS discount FROM trademarketing AS tm ', whereClause, ' ', groupbyClause);
END$$

DELIMITER ;
