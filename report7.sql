USE etlerdb;

DROP procedure IF EXISTS MARKETAnalysis;

DELIMITER $$

CREATE PROCEDURE MARKETAnalysis( IN nmgkUnit INT
                                , IN regionId INT
                                , IN cityId INT
                                , IN retailerId INT
                                , IN pointId INT
                                , IN firstDate DATE
                                , IN lastDate DATE
                                , OUT sequal VARCHAR(104096))
BEGIN
  DECLARE whereClause VARCHAR(5255) DEFAULT 'WHERE ';
  DECLARE groupbyClause VARCHAR(50) DEFAULT 'GROUP BY taskId, pointId, skuCategoryId';

  IF (nmgkUnit IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, 'nmgkUnit = ', nmgkUnit, ' AND ');
  END IF;

  SET whereClause = CONCAT(whereClause, '(timestamp BETWEEN ''', firstDate, ''' AND ''', lastDate, ''' )');

  IF (regionId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND regionId = ', regionId);
  END IF;

  IF (cityId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND cityId = ', cityId);
  END IF;

  IF (retailerId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND retailerId = ', retailerId);
  END IF;

  IF (pointId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND pointId = ', pointId);
  END IF;

  SET sequal = CONCAT('SELECT * FROM newcategories ', whereClause, groupbyClause);
END$$

DELIMITER ;
