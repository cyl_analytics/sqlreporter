#!/usr/bin/env bash

if [ -f reports.sql ]; then
  rm reports.sql
fi

cat report{8,1,2,4,5,6,7}.sql > reports.sql
