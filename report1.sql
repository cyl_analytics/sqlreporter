USE etlerdb;

DROP procedure IF EXISTS SHELFSHAREReport;

DELIMITER $$

CREATE PROCEDURE SHELFSHAREReport( IN nmgkUnit INT
                                 , IN regionId INT
                                 , IN cityId INT
                                 , IN retailerId INT
                                 , IN pointId INT
                                 , IN skuCategories VARCHAR(1755)
                                 , IN oosCategories VARCHAR(1755)
                                 , IN firstDate DATE
                                 , IN lastDate DATE
                                 , OUT brand_sql VARCHAR(54096)
                                 , OUT manufacturer_sql VARCHAR(54096)
                                 , OUT oos_sql VARCHAR(55096))
BEGIN
  DECLARE sqlss, sqloos VARCHAR(46972);
  DECLARE skuWhereClause VARCHAR(5755) DEFAULT 'WHERE ';
  DECLARE oosWhereClause VARCHAR(5755) DEFAULT 'WHERE ';


  IF (nmgkUnit IS NOT NULL) THEN
    SET skuWhereClause = CONCAT(skuWhereClause, 'sku.nmgkUnit = ', nmgkUnit, ' AND ');
    SET oosWhereClause = CONCAT(oosWhereClause, 'oos.nmgkUnit = ', nmgkUnit, ' AND ');
  END IF;

  SET skuWhereClause = CONCAT(skuWhereClause, '(sku.timestamp BETWEEN ''', firstDate, ''' AND ''', lastDate, ''' )');
  SET oosWhereClause = CONCAT(oosWhereClause, '(oos.timestamp BETWEEN ''', firstDate, ''' AND ''', lastDate, ''' )');

  IF (regionId IS NOT NULL) THEN
    SET skuWhereClause = CONCAT(skuWhereClause, ' AND sku.regionId = ', regionId);
    SET oosWhereClause = CONCAT(oosWhereClause, ' AND oos.regionId = ', regionId);
  END IF;

  IF (cityId IS NOT NULL) THEN
    SET skuWhereClause = CONCAT(skuWhereClause, ' AND sku.cityId = ', cityId);
    SET oosWhereClause = CONCAT(oosWhereClause, ' AND oos.cityId = ', cityId);
  END IF;

  IF (retailerId IS NOT NULL) THEN
    SET skuWhereClause = CONCAT(skuWhereClause, ' AND sku.retailerId = ', retailerId);
    SET oosWhereClause = CONCAT(oosWhereClause, ' AND oos.retailerId = ', retailerId);
  END IF;

  IF (pointId IS NOT NULL) THEN
    SET skuWhereClause = CONCAT(skuWhereClause, ' AND sku.pointId = ', pointId);
    SET oosWhereClause = CONCAT(oosWhereClause, ' AND oos.pointId = ', pointId);
  END IF;


  SELECT GROUP_CONCAT(DISTINCT(CONCAT('SUM(IF(timestamp = ''', timestamp, ''', svalue, NULL)) AS date_', DATE_FORMAT(timestamp, '%d_%m_%Y')))) INTO sqlss FROM shelfshare WHERE (timestamp BETWEEN firstDate AND lastDate);

  IF (skuCategories IS NOT NULL) THEN
    SET skuWhereClause = CONCAT(skuWhereClause, ' AND FIND_IN_SET(categoryId, ''', skuCategories ,''')');
  END IF;

  SET brand_sql = CONCAT('SELECT itemId, ', sqlss, ' FROM ((SELECT categoryId, itemId, T2.timestamp, value / T2.totalvalue AS svalue FROM shelfshare sku INNER JOIN (SELECT timestamp, sum(value) as totalvalue FROM shelfshare sku ', skuWhereClause, ' GROUP BY timestamp) T2 ON sku.timestamp = T2.timestamp ', skuWhereClause, ') AS orig) GROUP BY itemId;');

  SET manufacturer_sql = CONCAT('SELECT manufacturerId, ', sqlss, ' FROM ((SELECT categoryId, manufacturerId, t.timestamp, value / t.totalvalue AS svalue FROM shelfshare sku INNER JOIN (SELECT timestamp, sum(value) as totalvalue FROM shelfshare sku ', skuWhereClause, ' GROUP BY timestamp) AS t ON (sku.timestamp = t.timestamp) ', skuWhereClause, ') AS orig) GROUP BY manufacturerId;');

  SELECT GROUP_CONCAT(DISTINCT(CONCAT('OutOfStock(SUM(IF(timestamp = ''', timestamp, ''', value, NULL)), COUNT(IF(timestamp = ''', timestamp, ''', value, NULL))) AS date_', DATE_FORMAT(timestamp, '%d_%m_%Y')))) INTO sqloos FROM oos WHERE (timestamp BETWEEN firstDate AND lastDate);

  IF (oosCategories IS NOT NULL) THEN
    SET oosWhereClause = CONCAT(oosWhereClause, ' AND FIND_IN_SET(categoryId, ''', oosCategories,''')');
  END IF;

  SET oos_sql = CONCAT('SELECT ', sqloos, ' FROM oos ', oosWhereClause, ';');
END$$

DELIMITER ;
