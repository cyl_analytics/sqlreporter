USE etlerdb;

DROP procedure IF EXISTS OOSConsolidatedReport;

DELIMITER $$

CREATE PROCEDURE OOSConsolidatedReport( IN nmgkUnit INT
                                 , IN regionId INT
                                 , IN cityId INT
                                 , IN retailerId INT
                                 , IN pointId INT
                                 , IN oosCategories VARCHAR(1255)
                                 , IN firstDate DATE
                                 , IN lastDate DATE
                                 , OUT sequal VARCHAR(108096))
BEGIN
  DECLARE pivot_items VARCHAR(97083);
  DECLARE whereClause VARCHAR(5255) DEFAULT 'WHERE ';
  DECLARE groupbyClause VARCHAR(50) DEFAULT 'GROUP BY taskId, timestamp, pointId';

  IF (nmgkUnit IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, 'nmgkUnit = ', nmgkUnit, ' AND ');
  END IF;

  SET whereClause = CONCAT(whereClause, '(timestamp BETWEEN ''', firstDate, ''' AND ''', lastDate, ''' )');

  IF (regionId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND regionId = ', regionId);
  END IF;

  IF (cityId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND cityId = ', cityId);
  END IF;

  IF (retailerId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND retailerId = ', retailerId);
  END IF;

  IF (pointId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND pointId = ', pointId);
  END IF;


  IF (oosCategories IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND FIND_IN_SET(categoryId, ''', oosCategories, ''')');
    SELECT GROUP_CONCAT(DISTINCT(CONCAT('1 - SUM(IF(oos.itemId = ''', itemId, ''', oos.value, NULL)) AS sum_iid_', itemId))) INTO pivot_items FROM oos WHERE (timestamp BETWEEN firstDate AND lastDate) AND FIND_IN_SET(categoryId, oosCategories);
  ELSE SELECT GROUP_CONCAT(DISTINCT(CONCAT('1 - SUM(IF(oos.itemId = ''', itemId, ''', oos.value, NULL)) AS sum_iid_', itemId))) INTO pivot_items FROM oos WHERE (timestamp BETWEEN firstDate AND lastDate);
  END IF;

  SET sequal = CONCAT('SELECT * FROM ((SELECT taskId,userId,nmgkUnit,regionId,cityId,pointId,retailerId,timestamp,COUNT(value)-SUM(value) AS sku_count,OutOfStock(SUM(value), COUNT(value)) AS sku_percent FROM oos ', whereClause ,' ', groupbyClause,') AS OOSpart INNER JOIN (SELECT taskId,userId,nmgkUnit,regionId,cityId,pointId,retailerId,timestamp, ', pivot_items ,' FROM oos ', whereClause ,' ', groupbyClause,') AS NMGKsku ON (OOSpart.taskId = NMGKsku.taskId AND OOSpart.userId = NMGKsku.userId AND OOSpart.nmgkUnit = NMGKsku.nmgkUnit AND OOSpart.regionId = NMGKsku.regionId AND OOSpart.cityId = NMGKsku.cityId AND OOSpart.pointId = NMGKsku.pointId AND OOSpart.retailerId = NMGKsku.retailerId AND OOSpart.timestamp = NMGKsku.timestamp));');

END$$

DELIMITER ;
