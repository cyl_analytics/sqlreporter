USE etlerdb;

DROP procedure IF EXISTS SHELFSHARENmgkAndOtherReport;

DELIMITER $$

CREATE PROCEDURE SHELFSHARENmgkAndOtherReport( IN nmgkUnit INT
                                 , IN regionId INT
                                 , IN cityId INT
                                 , IN retailerId INT
                                 , IN pointId INT
                                 , IN skuCategories VARCHAR(1255)
                                 , IN oosCategories VARCHAR(1255)
                                 , IN firstDate DATE
                                 , IN lastDate DATE
                                 , OUT sequal VARCHAR(150096))
BEGIN
  DECLARE face_nmgk, face_other, ss_nmgk, ss_other VARCHAR(100072);
  DECLARE whereClause VARCHAR(1255) DEFAULT 'WHERE ';
  DECLARE oosWhereClause, ssWhereClause VARCHAR(5295);
  DECLARE groupbyClause VARCHAR(50) DEFAULT 'GROUP BY taskId, timestamp, pointId';
  DECLARE oosPart, nmgkFace, otherFace, nmgkSS, otherSS VARCHAR(12500);

  IF (nmgkUnit IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, 'nmgkUnit = ', nmgkUnit, ' AND ');
  END IF;

  SET whereClause = CONCAT(whereClause, '(timestamp BETWEEN ''', firstDate, ''' AND ''', lastDate, ''' )');

  IF (regionId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND regionId = ', regionId);
  END IF;

  IF (cityId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND cityId = ', cityId);
  END IF;

  IF (retailerId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND retailerId = ', retailerId);
  END IF;

  IF (pointId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND pointId = ', pointId);
  END IF;


  IF (oosCategories IS NOT NULL) THEN
    SET oosWhereClause = CONCAT(whereClause, ' AND FIND_IN_SET(categoryId, ''', oosCategories, ''')');
  ELSE SET oosWhereClause = whereClause;
  END IF;

  IF (skuCategories IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND FIND_IN_SET(categoryId, ''', skuCategories ,''')');

    SELECT GROUP_CONCAT(DISTINCT(CONCAT('SUM(IF(shelfshare.itemId = ''', itemId ,''', shelfshare.value, 0)) AS sum_iid_', itemId))) INTO face_nmgk FROM shelfshare WHERE (timestamp BETWEEN firstDate AND lastDate) AND manufacturerId = 75 AND FIND_IN_SET(categoryId, skuCategories);

    SELECT GROUP_CONCAT(DISTINCT(CONCAT('SUM(IF(shelfshare.itemId = ''', itemId ,''', shelfshare.value, 0)) AS sum_iid_', itemId))) INTO face_other FROM shelfshare WHERE (timestamp BETWEEN firstDate AND lastDate) AND manufacturerId <> 75 AND FIND_IN_SET(categoryId, skuCategories);

    SELECT GROUP_CONCAT(DISTINCT(CONCAT('ShelfShare(SUM(IF(shelfshare.itemId = ''', itemId ,''', shelfshare.value, 0)),SUM(IF(shelfshare.itemId <> ''', itemId ,''', shelfshare.value, 0))) AS ss_iid_', itemId))) INTO ss_nmgk FROM shelfshare WHERE (timestamp BETWEEN firstDate AND lastDate) AND manufacturerId = 75 AND FIND_IN_SET(categoryId, skuCategories);

    SELECT GROUP_CONCAT(DISTINCT(CONCAT('ShelfShare(SUM(IF(shelfshare.itemId = ''', itemId ,''', shelfshare.value, 0)),SUM(IF(shelfshare.itemId <> ''', itemId ,''', shelfshare.value, 0))) AS ss_iid_', itemId))) INTO ss_other FROM shelfshare WHERE (timestamp BETWEEN firstDate AND lastDate) AND manufacturerId <> 75 AND FIND_IN_SET(categoryId, skuCategories);
  ELSE SELECT GROUP_CONCAT(DISTINCT(CONCAT('SUM(IF(shelfshare.itemId = ''', itemId ,''', shelfshare.value, 0)) AS sum_iid_', itemId))) INTO face_nmgk FROM shelfshare WHERE (timestamp BETWEEN firstDate AND lastDate) AND manufacturerId = 75;

  SELECT GROUP_CONCAT(DISTINCT(CONCAT('SUM(IF(shelfshare.itemId = ''', itemId ,''', shelfshare.value, 0)) AS sum_iid_', itemId))) INTO face_other FROM shelfshare WHERE (timestamp BETWEEN firstDate AND lastDate) AND manufacturerId <> 75;

  SELECT GROUP_CONCAT(DISTINCT(CONCAT('ShelfShare(SUM(IF(shelfshare.itemId = ''', itemId ,''', shelfshare.value, 0)),SUM(IF(shelfshare.itemId <> ''', itemId ,''', shelfshare.value, 0))) AS ss_iid_', itemId))) INTO ss_nmgk FROM shelfshare WHERE (timestamp BETWEEN firstDate AND lastDate) AND manufacturerId = 75;

  SELECT GROUP_CONCAT(DISTINCT(CONCAT('ShelfShare(SUM(IF(shelfshare.itemId = ''', itemId ,''', shelfshare.value, 0)),SUM(IF(shelfshare.itemId <> ''', itemId ,''', shelfshare.value, 0))) AS ss_iid_', itemId))) INTO ss_other FROM shelfshare WHERE (timestamp BETWEEN firstDate AND lastDate) AND manufacturerId <> 75;
  END IF;

  SET oosPart = CONCAT('SELECT taskId,userId,nmgkUnit,regionId,cityId,pointId,retailerId,timestamp,COUNT(value)-SUM(value) AS sku_count,OutOfStock(SUM(value), COUNT(value)) AS sku_precent FROM oos ', oosWhereClause ,' ', groupbyClause);

  SET nmgkFace = CONCAT('SELECT taskId,userId,nmgkUnit,regionId,cityId,pointId,retailerId,timestamp, ', face_nmgk,', SUM(value) as face_nmgk_total FROM shelfshare ', whereClause ,' AND shelfshare.manufacturerId = 75 ', groupbyClause);

  SET otherFace = CONCAT('SELECT taskId,userId,nmgkUnit,regionId,cityId,pointId,retailerId,timestamp, ', face_other,', SUM(value) as face_other_total FROM shelfshare ', whereClause ,' AND shelfshare.manufacturerId <> 75 ', groupbyClause);

  SET nmgkSS = CONCAT('SELECT taskId,userId,nmgkUnit,regionId,cityId,pointId,retailerId,timestamp, ', ss_nmgk,', ShelfShare(SUM(IF(shelfshare.manufacturerId = 75, shelfshare.value, 0)), SUM(IF(shelfshare.manufacturerId <> 75, shelfshare.value, 0))) as ss_nmgk_total FROM shelfshare ', whereClause ,' ', groupbyClause);

  SET otherSS = CONCAT('SELECT taskId,userId,nmgkUnit,regionId,cityId,pointId,retailerId,timestamp, ', ss_other,', ShelfShare(SUM(IF(shelfshare.manufacturerId <> 75, shelfshare.value, 0)), SUM(IF(shelfshare.manufacturerId = 75, shelfshare.value, 0))) as ss_other_total FROM shelfshare ', whereClause ,' ', groupbyClause);

  SET sequal = CONCAT('SELECT * FROM ((', oosPart, ') AS OOSpart
                       INNER JOIN (', nmgkFace, ') AS NMGKface
                         ON (OOSpart.taskId = NMGKface.taskId AND OOSpart.userId = NMGKface.userId AND OOSpart.nmgkUnit = NMGKface.nmgkUnit AND
                             OOSpart.regionId = NMGKface.regionId AND OOSpart.cityId = NMGKface.cityId AND OOSpart.pointId = NMGKface.pointId AND
                             OOSpart.retailerId = NMGKface.retailerId AND OOSpart.timestamp = NMGKface.timestamp)
                       INNER JOIN (', otherFace, ') AS OTHERface
                         ON (OOSpart.userId = OTHERface.userId AND OOSpart.nmgkUnit = OTHERface.nmgkUnit AND OOSpart.regionId = OTHERface.regionId AND
                             OOSpart.cityId = OTHERface.cityId AND OOSpart.pointId = OTHERface.pointId AND OOSpart.retailerId = OTHERface.retailerId AND
                             OOSpart.timestamp = OTHERface.timestamp AND OOSpart.taskId = OTHERface.taskId)
                       INNER JOIN (', nmgkSS, ') AS NMGKss
                         ON (OOSpart.userId = NMGKss.userId AND OOSpart.nmgkUnit = NMGKss.nmgkUnit AND OOSpart.regionId = NMGKss.regionId AND
                             OOSpart.cityId = NMGKss.cityId AND OOSpart.pointId = NMGKss.pointId AND OOSpart.retailerId = NMGKss.retailerId AND
                             OOSpart.timestamp = NMGKss.timestamp AND OOSpart.taskId = NMGKss.taskId)
                       INNER JOIN (', otherSS, ') AS OTHERss
                         ON (OOSpart.userId = OTHERss.userId AND OOSpart.nmgkUnit = OTHERss.nmgkUnit AND OOSpart.regionId = OTHERss.regionId AND
                             OOSpart.cityId = OTHERss.cityId AND OOSpart.pointId = OTHERss.pointId AND OOSpart.retailerId = OTHERss.retailerId AND
                             OOSpart.timestamp = OTHERss.timestamp AND OOSpart.taskId = OTHERss.taskId));');

END$$

DELIMITER ;
