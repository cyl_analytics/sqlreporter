USE etlerdbv1;

DROP function IF EXISTS OutOfStock;
DROP function IF EXISTS ShelfShare;
DROP procedure IF EXISTS KPIReport;

DELIMITER $$

CREATE FUNCTION OutOfStock(sums DOUBLE, counts DOUBLE) RETURNS DOUBLE
    DETERMINISTIC
BEGIN
  RETURN ROUND(100 * ((counts - sums) / counts), 2);
END$$

CREATE FUNCTION ShelfShare(choice_sums DOUBLE, other_sums DOUBLE) RETURNS DOUBLE
    DETERMINISTIC
BEGIN
  RETURN ROUND(100 * (choice_sums / (choice_sums + other_sums)), 2);
END$$


CREATE PROCEDURE KPIReport( IN nmgkUnit INT
                          , IN startDate DATE
                          , IN finishDate DATE
                          , IN regionId INT
                          , IN retailerId INT
                          , IN userId INT
                          , IN oosCategories VARCHAR(3255)
                          , IN ssCategories VARCHAR(3255)
                          , OUT sequal VARCHAR(204096))
  BEGIN
    DECLARE sqloos,sqlss VARCHAR(53072);
    DECLARE t1, t2, t3 VARCHAR(60000);
    DECLARE leftAll, middleAll, rightAll VARCHAR(80000);
    DECLARE groupbyClause VARCHAR(50) DEFAULT 'GROUP BY timestamp, userId, taskId, pointId';
    DECLARE whereClause VARCHAR(5255) DEFAULT 'WHERE ';
    DECLARE oosWhereClause, ssWhereClause VARCHAR(6300);
    DECLARE sqloosColumnName VARCHAR(5000);
    DECLARE sqlssColumnName VARCHAR(5000);


    IF (nmgkUnit IS NOT NULL) THEN
      SET whereClause = CONCAT(whereClause, 'nmgkUnit = ', nmgkUnit, ' AND ');
    END IF;

    SET whereClause = CONCAT(whereClause, '(timestamp BETWEEN ''', startDate, ''' AND ''', finishDate, ''' )');

    IF (regionId IS NOT NULL) THEN
      SET whereClause = CONCAT(whereClause, ' AND regionId = ', regionId);
    END IF;

    IF (retailerId IS NOT NULL) THEN
      SET whereClause = CONCAT(whereClause, ' AND retailerId = ', retailerId);
    END IF;

    IF (userId IS NOT NULL) THEN
      SET whereClause = CONCAT(whereClause, ' AND userId = ', userId);
    END IF;

    IF (oosCategories IS NOT NULL) THEN
        SELECT GROUP_CONCAT(DISTINCT(CONCAT('OutOfStock(SUM(IF(oos.categoryId = ''', categoryId, ''', oos.value, NULL)), COUNT(IF(oos.categoryId = ''', categoryId, ''', oos.value, NULL))) AS oos_cid_', categoryId))) INTO sqloos FROM oos WHERE (timestamp BETWEEN startDate AND finishDate) AND FIND_IN_SET(categoryId, oosCategories);
        
        SELECT GROUP_CONCAT(DISTINCT(CONCAT('oos_cid_', categoryId))) INTO sqloosColumnName FROM oos WHERE (timestamp BETWEEN startDate AND finishDate) AND FIND_IN_SET(categoryId, oosCategories);

        SET oosWhereClause = CONCAT(whereClause, ' AND FIND_IN_SET(oos.categoryId,''', oosCategories, ''')');
       
      ELSE SELECT GROUP_CONCAT(DISTINCT(CONCAT('OutOfStock(SUM(IF(oos.categoryId = ''', categoryId, ''', oos.value, NULL)), COUNT(IF(oos.categoryId = ''', categoryId, ''', oos.value, NULL))) AS oos_cid_', categoryId))) INTO sqloos FROM oos WHERE timestamp BETWEEN startDate AND finishDate;

        SELECT GROUP_CONCAT(DISTINCT(CONCAT('oos_cid_', categoryId))) INTO sqloosColumnName FROM oos WHERE timestamp BETWEEN startDate AND finishDate;

        SET oosWhereClause = whereClause;
    END IF;

    IF (ssCategories IS NOT NULL) THEN
       SELECT GROUP_CONCAT(DISTINCT(CONCAT('ShelfShare(SUM(IF(shelfshare.categoryId = ''', categoryId, ''', IF(shelfshare.manufacturerId = ''', 75, ''', shelfshare.value, 0), NULL)), SUM(IF(shelfshare.categoryId = ''', categoryId, ''', IF(shelfshare.manufacturerId <> ''', 75, ''', shelfshare.value, 0), NULL))) AS ss_cid_', categoryId))) INTO sqlss FROM shelfshare WHERE (timestamp BETWEEN startDate AND finishDate) AND FIND_IN_SET(categoryId, ssCategories);

       SELECT GROUP_CONCAT(DISTINCT(CONCAT('ss_cid_', categoryId))) INTO sqlssColumnName FROM shelfshare WHERE (timestamp BETWEEN startDate AND finishDate) AND FIND_IN_SET(categoryId, ssCategories);

       SET ssWhereClause = CONCAT(whereClause, ' AND FIND_IN_SET(shelfshare.categoryId, ''', ssCategories, ''')');
      
    ELSE SELECT GROUP_CONCAT(DISTINCT(CONCAT('ShelfShare(SUM(IF(shelfshare.categoryId = ''', categoryId, ''', IF(shelfshare.manufacturerId = ''', 75, ''', shelfshare.value, 0), NULL)), SUM(IF(shelfshare.categoryId = ''', categoryId, ''', IF(shelfshare.manufacturerId <> ''', 75, ''', shelfshare.value, 0), NULL))) AS ss_cid_', categoryId))) INTO sqlss FROM shelfshare WHERE (timestamp BETWEEN startDate AND finishDate);

       SELECT GROUP_CONCAT(DISTINCT(CONCAT('ss_cid_', categoryId))) INTO sqlssColumnName FROM shelfshare WHERE (timestamp BETWEEN startDate AND finishDate);

       SET ssWhereClause = whereClause;
    END IF;

    SET t1 = CONCAT('(SELECT userId, timestamp, taskId, nmgkUnit, regionId, cityId, pointId, retailerId, OutOfStock(SUM(oos.value), COUNT(oos.value)) as oos_nmgk_total, ', sqloos, ' FROM oos ', oosWhereClause, ' ', groupbyClause,') AS T1');

    SET t2 = CONCAT('(SELECT userId, timestamp, taskId, nmgkUnit, regionId, cityId, pointId, retailerId, ShelfShare(SUM(IF(shelfshare.manufacturerId = 75, shelfshare.value, NULL)), SUM(IF(shelfshare.manufacturerId <> 75, shelfshare.value, NULL))) as ss_nmgk_total, ', sqlss, ' FROM shelfshare ', ssWhereClause, ' ', groupbyClause,') AS T2');

    SET t3 = CONCAT('(SELECT userId, timestamp, taskId, nmgkUnit, regionId, cityId, pointId, retailerId, GROUP_CONCAT(IF(abphoto.type = ''', "photo_before", ''', abphoto.value, NULL)) AS photo_before, GROUP_CONCAT(IF(abphoto.type = ''', "photo_after", ''', abphoto.value,NULL)) AS photo_after FROM abphoto ', whereClause,' ', groupbyClause,') AS T3');

    SET leftAll = CONCAT('SELECT T1.userId,T1.timestamp,T1.taskId,T1.nmgkUnit,T1.regionId,T1.cityId,T1.pointId,T1.retailerId,T1.oos_nmgk_total,', sqloosColumnName, ',', sqlssColumnName,',T3.photo_before, T3.photo_after FROM (', t1, ' LEFT OUTER JOIN ', t2, ' ON (T1.taskId = T2.taskId) LEFT OUTER JOIN ', t3, ' ON (T1.taskId = T3.taskId))');
    SET middleAll = CONCAT('SELECT T2.userId,T2.timestamp,T2.taskId,T2.nmgkUnit,T2.regionId,T2.cityId,T2.pointId,T2.retailerId,T1.oos_nmgk_total,', sqloosColumnName, ',', sqlssColumnName,',T3.photo_before, T3.photo_after FROM (', t1, ' RIGHT OUTER JOIN ', t2, ' ON (T2.taskId = T1.taskId) LEFT OUTER JOIN ', t3, ' ON (T2.taskId = T3.taskId))');
    SET rightAll = CONCAT('SELECT T3.userId,T3.timestamp,T3.taskId,T3.nmgkUnit,T3.regionId,T3.cityId,T3.pointId,T3.retailerId,T1.oos_nmgk_total,', sqloosColumnName, ',', sqlssColumnName,',T3.photo_before, T3.photo_after FROM (', t1, ' RIGHT OUTER JOIN ', t2, ' ON (T2.taskId = T1.taskId) RIGHT OUTER JOIN ', t3, ' ON (T3.taskId = T2.taskId))');

    SET sequal = CONCAT(leftALL, ' UNION ', middleALL, ' UNION ', rightALL, ';');

  END$$

DELIMITER ;
 
USE etlerdbv1;

DROP procedure IF EXISTS SHELFSHAREReport;

DELIMITER $$

CREATE PROCEDURE SHELFSHAREReport( IN nmgkUnit INT
                                 , IN regionId INT
                                 , IN cityId INT
                                 , IN retailerId INT
                                 , IN pointId INT
                                 , IN skuCategories VARCHAR(1755)
                                 , IN oosCategories VARCHAR(1755)
                                 , IN firstDate DATE
                                 , IN lastDate DATE
                                 , OUT brand_sql VARCHAR(54096)
                                 , OUT manufacturer_sql VARCHAR(54096)
                                 , OUT oos_sql VARCHAR(55096))
BEGIN
  DECLARE sqlss, sqloos VARCHAR(46972);
  DECLARE skuWhereClause VARCHAR(5755) DEFAULT 'WHERE ';
  DECLARE oosWhereClause VARCHAR(5755) DEFAULT 'WHERE ';


  IF (nmgkUnit IS NOT NULL) THEN
    SET skuWhereClause = CONCAT(skuWhereClause, 'sku.nmgkUnit = ', nmgkUnit, ' AND ');
    SET oosWhereClause = CONCAT(oosWhereClause, 'oos.nmgkUnit = ', nmgkUnit, ' AND ');
  END IF;

  SET skuWhereClause = CONCAT(skuWhereClause, '(sku.timestamp BETWEEN ''', firstDate, ''' AND ''', lastDate, ''' )');
  SET oosWhereClause = CONCAT(oosWhereClause, '(oos.timestamp BETWEEN ''', firstDate, ''' AND ''', lastDate, ''' )');

  IF (regionId IS NOT NULL) THEN
    SET skuWhereClause = CONCAT(skuWhereClause, ' AND sku.regionId = ', regionId);
    SET oosWhereClause = CONCAT(oosWhereClause, ' AND oos.regionId = ', regionId);
  END IF;

  IF (cityId IS NOT NULL) THEN
    SET skuWhereClause = CONCAT(skuWhereClause, ' AND sku.cityId = ', cityId);
    SET oosWhereClause = CONCAT(oosWhereClause, ' AND oos.cityId = ', cityId);
  END IF;

  IF (retailerId IS NOT NULL) THEN
    SET skuWhereClause = CONCAT(skuWhereClause, ' AND sku.retailerId = ', retailerId);
    SET oosWhereClause = CONCAT(oosWhereClause, ' AND oos.retailerId = ', retailerId);
  END IF;

  IF (pointId IS NOT NULL) THEN
    SET skuWhereClause = CONCAT(skuWhereClause, ' AND sku.pointId = ', pointId);
    SET oosWhereClause = CONCAT(oosWhereClause, ' AND oos.pointId = ', pointId);
  END IF;


  SELECT GROUP_CONCAT(DISTINCT(CONCAT('SUM(IF(timestamp = ''', timestamp, ''', svalue, NULL)) AS date_', DATE_FORMAT(timestamp, '%d_%m_%Y')))) INTO sqlss FROM shelfshare WHERE (timestamp BETWEEN firstDate AND lastDate);

  IF (skuCategories IS NOT NULL) THEN
    SET skuWhereClause = CONCAT(skuWhereClause, ' AND FIND_IN_SET(categoryId, ''', skuCategories ,''')');
  END IF;

  SET brand_sql = CONCAT('SELECT itemId, ', sqlss, ' FROM ((SELECT categoryId, itemId, T2.timestamp, value / T2.totalvalue AS svalue FROM shelfshare sku INNER JOIN (SELECT timestamp, sum(value) as totalvalue FROM shelfshare sku ', skuWhereClause, ' GROUP BY timestamp) T2 ON sku.timestamp = T2.timestamp ', skuWhereClause, ') AS orig) GROUP BY itemId;');

  SET manufacturer_sql = CONCAT('SELECT manufacturerId, ', sqlss, ' FROM ((SELECT categoryId, manufacturerId, t.timestamp, value / t.totalvalue AS svalue FROM shelfshare sku INNER JOIN (SELECT timestamp, sum(value) as totalvalue FROM shelfshare sku ', skuWhereClause, ' GROUP BY timestamp) AS t ON (sku.timestamp = t.timestamp) ', skuWhereClause, ') AS orig) GROUP BY manufacturerId;');

  SELECT GROUP_CONCAT(DISTINCT(CONCAT('OutOfStock(SUM(IF(timestamp = ''', timestamp, ''', value, NULL)), COUNT(IF(timestamp = ''', timestamp, ''', value, NULL))) AS date_', DATE_FORMAT(timestamp, '%d_%m_%Y')))) INTO sqloos FROM oos WHERE (timestamp BETWEEN firstDate AND lastDate);

  IF (oosCategories IS NOT NULL) THEN
    SET oosWhereClause = CONCAT(oosWhereClause, ' AND FIND_IN_SET(categoryId, ''', oosCategories,''')');
  END IF;

  SET oos_sql = CONCAT('SELECT ', sqloos, ' FROM oos ', oosWhereClause, ';');
END$$

DELIMITER ;
USE etlerdbv1;

DROP procedure IF EXISTS SHELFSHARENmgkAndOtherReport;

DELIMITER $$

CREATE PROCEDURE SHELFSHARENmgkAndOtherReport( IN nmgkUnit INT
                                 , IN regionId INT
                                 , IN cityId INT
                                 , IN retailerId INT
                                 , IN pointId INT
                                 , IN skuCategories VARCHAR(1255)
                                 , IN oosCategories VARCHAR(1255)
                                 , IN firstDate DATE
                                 , IN lastDate DATE
                                 , OUT sequal VARCHAR(150096))
BEGIN
  DECLARE face_nmgk, face_other, ss_nmgk, ss_other VARCHAR(100072);
  DECLARE whereClause VARCHAR(1255) DEFAULT 'WHERE ';
  DECLARE oosWhereClause, ssWhereClause VARCHAR(5295);
  DECLARE groupbyClause VARCHAR(50) DEFAULT 'GROUP BY taskId, timestamp, pointId';
  DECLARE oosPart, nmgkFace, otherFace, nmgkSS, otherSS VARCHAR(12500);

  IF (nmgkUnit IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, 'nmgkUnit = ', nmgkUnit, ' AND ');
  END IF;

  SET whereClause = CONCAT(whereClause, '(timestamp BETWEEN ''', firstDate, ''' AND ''', lastDate, ''' )');

  IF (regionId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND regionId = ', regionId);
  END IF;

  IF (cityId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND cityId = ', cityId);
  END IF;

  IF (retailerId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND retailerId = ', retailerId);
  END IF;

  IF (pointId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND pointId = ', pointId);
  END IF;


  IF (oosCategories IS NOT NULL) THEN
    SET oosWhereClause = CONCAT(whereClause, ' AND FIND_IN_SET(categoryId, ''', oosCategories, ''')');
  ELSE SET oosWhereClause = whereClause;
  END IF;

  IF (skuCategories IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND FIND_IN_SET(categoryId, ''', skuCategories ,''')');

    SELECT GROUP_CONCAT(DISTINCT(CONCAT('SUM(IF(shelfshare.itemId = ''', itemId ,''', shelfshare.value, 0)) AS sum_iid_', itemId))) INTO face_nmgk FROM shelfshare WHERE (timestamp BETWEEN firstDate AND lastDate) AND manufacturerId = 75 AND FIND_IN_SET(categoryId, skuCategories);

    SELECT GROUP_CONCAT(DISTINCT(CONCAT('SUM(IF(shelfshare.itemId = ''', itemId ,''', shelfshare.value, 0)) AS sum_iid_', itemId))) INTO face_other FROM shelfshare WHERE (timestamp BETWEEN firstDate AND lastDate) AND manufacturerId <> 75 AND FIND_IN_SET(categoryId, skuCategories);

    SELECT GROUP_CONCAT(DISTINCT(CONCAT('ShelfShare(SUM(IF(shelfshare.itemId = ''', itemId ,''', shelfshare.value, 0)),SUM(IF(shelfshare.itemId <> ''', itemId ,''', shelfshare.value, 0))) AS ss_iid_', itemId))) INTO ss_nmgk FROM shelfshare WHERE (timestamp BETWEEN firstDate AND lastDate) AND manufacturerId = 75 AND FIND_IN_SET(categoryId, skuCategories);

    SELECT GROUP_CONCAT(DISTINCT(CONCAT('ShelfShare(SUM(IF(shelfshare.itemId = ''', itemId ,''', shelfshare.value, 0)),SUM(IF(shelfshare.itemId <> ''', itemId ,''', shelfshare.value, 0))) AS ss_iid_', itemId))) INTO ss_other FROM shelfshare WHERE (timestamp BETWEEN firstDate AND lastDate) AND manufacturerId <> 75 AND FIND_IN_SET(categoryId, skuCategories);
  ELSE SELECT GROUP_CONCAT(DISTINCT(CONCAT('SUM(IF(shelfshare.itemId = ''', itemId ,''', shelfshare.value, 0)) AS sum_iid_', itemId))) INTO face_nmgk FROM shelfshare WHERE (timestamp BETWEEN firstDate AND lastDate) AND manufacturerId = 75;

  SELECT GROUP_CONCAT(DISTINCT(CONCAT('SUM(IF(shelfshare.itemId = ''', itemId ,''', shelfshare.value, 0)) AS sum_iid_', itemId))) INTO face_other FROM shelfshare WHERE (timestamp BETWEEN firstDate AND lastDate) AND manufacturerId <> 75;

  SELECT GROUP_CONCAT(DISTINCT(CONCAT('ShelfShare(SUM(IF(shelfshare.itemId = ''', itemId ,''', shelfshare.value, 0)),SUM(IF(shelfshare.itemId <> ''', itemId ,''', shelfshare.value, 0))) AS ss_iid_', itemId))) INTO ss_nmgk FROM shelfshare WHERE (timestamp BETWEEN firstDate AND lastDate) AND manufacturerId = 75;

  SELECT GROUP_CONCAT(DISTINCT(CONCAT('ShelfShare(SUM(IF(shelfshare.itemId = ''', itemId ,''', shelfshare.value, 0)),SUM(IF(shelfshare.itemId <> ''', itemId ,''', shelfshare.value, 0))) AS ss_iid_', itemId))) INTO ss_other FROM shelfshare WHERE (timestamp BETWEEN firstDate AND lastDate) AND manufacturerId <> 75;
  END IF;

  SET oosPart = CONCAT('SELECT taskId,userId,nmgkUnit,regionId,cityId,pointId,retailerId,timestamp,COUNT(value)-SUM(value) AS sku_count,OutOfStock(SUM(value), COUNT(value)) AS sku_precent FROM oos ', oosWhereClause ,' ', groupbyClause);

  SET nmgkFace = CONCAT('SELECT taskId,userId,nmgkUnit,regionId,cityId,pointId,retailerId,timestamp, ', face_nmgk,', SUM(value) as face_nmgk_total FROM shelfshare ', whereClause ,' AND shelfshare.manufacturerId = 75 ', groupbyClause);

  SET otherFace = CONCAT('SELECT taskId,userId,nmgkUnit,regionId,cityId,pointId,retailerId,timestamp, ', face_other,', SUM(value) as face_other_total FROM shelfshare ', whereClause ,' AND shelfshare.manufacturerId <> 75 ', groupbyClause);

  SET nmgkSS = CONCAT('SELECT taskId,userId,nmgkUnit,regionId,cityId,pointId,retailerId,timestamp, ', ss_nmgk,', ShelfShare(SUM(IF(shelfshare.manufacturerId = 75, shelfshare.value, 0)), SUM(IF(shelfshare.manufacturerId <> 75, shelfshare.value, 0))) as ss_nmgk_total FROM shelfshare ', whereClause ,' ', groupbyClause);

  SET otherSS = CONCAT('SELECT taskId,userId,nmgkUnit,regionId,cityId,pointId,retailerId,timestamp, ', ss_other,', ShelfShare(SUM(IF(shelfshare.manufacturerId <> 75, shelfshare.value, 0)), SUM(IF(shelfshare.manufacturerId = 75, shelfshare.value, 0))) as ss_other_total FROM shelfshare ', whereClause ,' ', groupbyClause);

  SET sequal = CONCAT('SELECT * FROM ((', oosPart, ') AS OOSpart
                       INNER JOIN (', nmgkFace, ') AS NMGKface
                         ON (OOSpart.taskId = NMGKface.taskId AND OOSpart.userId = NMGKface.userId AND OOSpart.nmgkUnit = NMGKface.nmgkUnit AND
                             OOSpart.regionId = NMGKface.regionId AND OOSpart.cityId = NMGKface.cityId AND OOSpart.pointId = NMGKface.pointId AND
                             OOSpart.retailerId = NMGKface.retailerId AND OOSpart.timestamp = NMGKface.timestamp)
                       INNER JOIN (', otherFace, ') AS OTHERface
                         ON (OOSpart.userId = OTHERface.userId AND OOSpart.nmgkUnit = OTHERface.nmgkUnit AND OOSpart.regionId = OTHERface.regionId AND
                             OOSpart.cityId = OTHERface.cityId AND OOSpart.pointId = OTHERface.pointId AND OOSpart.retailerId = OTHERface.retailerId AND
                             OOSpart.timestamp = OTHERface.timestamp AND OOSpart.taskId = OTHERface.taskId)
                       INNER JOIN (', nmgkSS, ') AS NMGKss
                         ON (OOSpart.userId = NMGKss.userId AND OOSpart.nmgkUnit = NMGKss.nmgkUnit AND OOSpart.regionId = NMGKss.regionId AND
                             OOSpart.cityId = NMGKss.cityId AND OOSpart.pointId = NMGKss.pointId AND OOSpart.retailerId = NMGKss.retailerId AND
                             OOSpart.timestamp = NMGKss.timestamp AND OOSpart.taskId = NMGKss.taskId)
                       INNER JOIN (', otherSS, ') AS OTHERss
                         ON (OOSpart.userId = OTHERss.userId AND OOSpart.nmgkUnit = OTHERss.nmgkUnit AND OOSpart.regionId = OTHERss.regionId AND
                             OOSpart.cityId = OTHERss.cityId AND OOSpart.pointId = OTHERss.pointId AND OOSpart.retailerId = OTHERss.retailerId AND
                             OOSpart.timestamp = OTHERss.timestamp AND OOSpart.taskId = OTHERss.taskId));');

END$$

DELIMITER ;
USE etlerdbv1;

DROP procedure IF EXISTS OOSConsolidatedReport;

DELIMITER $$

CREATE PROCEDURE OOSConsolidatedReport( IN nmgkUnit INT
                                 , IN regionId INT
                                 , IN cityId INT
                                 , IN retailerId INT
                                 , IN pointId INT
                                 , IN oosCategories VARCHAR(1255)
                                 , IN firstDate DATE
                                 , IN lastDate DATE
                                 , OUT sequal VARCHAR(108096))
BEGIN
  DECLARE pivot_items VARCHAR(97083);
  DECLARE whereClause VARCHAR(5255) DEFAULT 'WHERE ';
  DECLARE groupbyClause VARCHAR(50) DEFAULT 'GROUP BY taskId, timestamp, pointId';

  IF (nmgkUnit IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, 'nmgkUnit = ', nmgkUnit, ' AND ');
  END IF;

  SET whereClause = CONCAT(whereClause, '(timestamp BETWEEN ''', firstDate, ''' AND ''', lastDate, ''' )');

  IF (regionId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND regionId = ', regionId);
  END IF;

  IF (cityId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND cityId = ', cityId);
  END IF;

  IF (retailerId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND retailerId = ', retailerId);
  END IF;

  IF (pointId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND pointId = ', pointId);
  END IF;


  IF (oosCategories IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND FIND_IN_SET(categoryId, ''', oosCategories, ''')');
    SELECT GROUP_CONCAT(DISTINCT(CONCAT('1 - SUM(IF(oos.itemId = ''', itemId, ''', oos.value, NULL)) AS sum_iid_', itemId))) INTO pivot_items FROM oos WHERE (timestamp BETWEEN firstDate AND lastDate) AND FIND_IN_SET(categoryId, oosCategories);
  ELSE SELECT GROUP_CONCAT(DISTINCT(CONCAT('1 - SUM(IF(oos.itemId = ''', itemId, ''', oos.value, NULL)) AS sum_iid_', itemId))) INTO pivot_items FROM oos WHERE (timestamp BETWEEN firstDate AND lastDate);
  END IF;

  SET sequal = CONCAT('SELECT * FROM ((SELECT taskId,userId,nmgkUnit,regionId,cityId,pointId,retailerId,timestamp,COUNT(value)-SUM(value) AS sku_count,OutOfStock(SUM(value), COUNT(value)) AS sku_percent FROM oos ', whereClause ,' ', groupbyClause,') AS OOSpart INNER JOIN (SELECT taskId,userId,nmgkUnit,regionId,cityId,pointId,retailerId,timestamp, ', pivot_items ,' FROM oos ', whereClause ,' ', groupbyClause,') AS NMGKsku ON (OOSpart.taskId = NMGKsku.taskId AND OOSpart.userId = NMGKsku.userId AND OOSpart.nmgkUnit = NMGKsku.nmgkUnit AND OOSpart.regionId = NMGKsku.regionId AND OOSpart.cityId = NMGKsku.cityId AND OOSpart.pointId = NMGKsku.pointId AND OOSpart.retailerId = NMGKsku.retailerId AND OOSpart.timestamp = NMGKsku.timestamp));');

END$$

DELIMITER ;
USE etlerdbv1;

DROP procedure IF EXISTS PRICEMonitoring;

DELIMITER $$

CREATE PROCEDURE PRICEMonitoring( IN nmgkUnit INT
                                , IN regionId INT
                                , IN cityId INT
                                , IN retailerId INT
                                , IN pointId INT
                                , IN firstDate DATE
                                , IN lastDate DATE
                                , OUT sequal VARCHAR(104096))
BEGIN
  DECLARE whereClause VARCHAR(5255) DEFAULT 'WHERE ';
  DECLARE groupbyClause VARCHAR(50) DEFAULT 'GROUP BY taskId, pointId, skuId';

  IF (nmgkUnit IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, 'nmgkUnit = ', nmgkUnit, ' AND ');
  END IF;

  SET whereClause = CONCAT(whereClause, '(timestamp BETWEEN ''', firstDate, ''' AND ''', lastDate, ''' )');

  IF (regionId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND regionId = ', regionId);
  END IF;

  IF (cityId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND cityId = ', cityId);
  END IF;

  IF (retailerId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND retailerId = ', retailerId);
  END IF;

  IF (pointId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND pointId = ', pointId);
  END IF;

  SET sequal = CONCAT('SELECT userId,timestamp,taskId,nmgkUnit,regionId,cityId,pointId,nmgkTtType,retailerId,skuId,MAX(price) AS price FROM pricemarketing ', whereClause, groupbyClause);
END$$

DELIMITER ;
USE etlerdbv1;

DROP procedure IF EXISTS PROMOMonitoring;

DELIMITER $$

CREATE PROCEDURE PROMOMonitoring( IN nmgkUnit INT
                                , IN regionId INT
                                , IN cityId INT
                                , IN retailerId INT
                                , IN pointId INT
                                , IN firstDate DATE
                                , IN lastDate DATE
                                , OUT sequal VARCHAR(104096))
BEGIN
  DECLARE pivot_campaigns VARCHAR(93072);
  DECLARE whereClause VARCHAR(5255) DEFAULT 'WHERE ';
  DECLARE groupbyClause VARCHAR(50) DEFAULT 'GROUP BY taskId, pointId, skuId';

  IF (nmgkUnit IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, 'nmgkUnit = ', nmgkUnit, ' AND ');
  END IF;

  SET whereClause = CONCAT(whereClause, '(timestamp BETWEEN ''', firstDate, ''' AND ''', lastDate, ''' )');

  IF (regionId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND regionId = ', regionId);
  END IF;

  IF (cityId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND cityId = ', cityId);
  END IF;

  IF (retailerId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND retailerId = ', retailerId);
  END IF;

  IF (pointId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND pointId = ', pointId);
  END IF;

  SELECT GROUP_CONCAT(DISTINCT(CONCAT('GROUP_CONCAT(IF(tm.campaign = ''', campaign , ''', 1, NULL)) AS campaign_', campaign))) INTO pivot_campaigns FROM trademarketing WHERE (timestamp BETWEEN firstDate AND lastDate);

  SET sequal = CONCAT('SELECT userId, timestamp, taskId, nmgkUnit, regionId, cityId, pointId, nmgkTtType, retailerId, skuId, ', pivot_campaigns, ', MAX(price) AS price, MAX(priceBefore) AS priceBefore, MAX(discount) AS discount FROM trademarketing AS tm ', whereClause, ' ', groupbyClause);
END$$

DELIMITER ;
USE etlerdbv1;

DROP procedure IF EXISTS MARKETAnalysis;

DELIMITER $$

CREATE PROCEDURE MARKETAnalysis( IN nmgkUnit INT
                                , IN regionId INT
                                , IN cityId INT
                                , IN retailerId INT
                                , IN pointId INT
                                , IN firstDate DATE
                                , IN lastDate DATE
                                , OUT sequal VARCHAR(104096))
BEGIN
  DECLARE whereClause VARCHAR(5255) DEFAULT 'WHERE ';
  DECLARE groupbyClause VARCHAR(50) DEFAULT 'GROUP BY taskId, pointId, skuCategoryId';

  IF (nmgkUnit IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, 'nmgkUnit = ', nmgkUnit, ' AND ');
  END IF;

  SET whereClause = CONCAT(whereClause, '(timestamp BETWEEN ''', firstDate, ''' AND ''', lastDate, ''' )');

  IF (regionId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND regionId = ', regionId);
  END IF;

  IF (cityId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND cityId = ', cityId);
  END IF;

  IF (retailerId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND retailerId = ', retailerId);
  END IF;

  IF (pointId IS NOT NULL) THEN
    SET whereClause = CONCAT(whereClause, ' AND pointId = ', pointId);
  END IF;

  SET sequal = CONCAT('SELECT * FROM newcategories ', whereClause, groupbyClause);
END$$

DELIMITER ;
