USE etlerdb;

DROP function IF EXISTS OutOfStock;
DROP function IF EXISTS ShelfShare;
DROP procedure IF EXISTS KPIReport;

DELIMITER $$

CREATE FUNCTION OutOfStock(sums DOUBLE, counts DOUBLE) RETURNS DOUBLE
    DETERMINISTIC
BEGIN
  RETURN ROUND(100 * ((counts - sums) / counts), 2);
END$$

CREATE FUNCTION ShelfShare(choice_sums DOUBLE, other_sums DOUBLE) RETURNS DOUBLE
    DETERMINISTIC
BEGIN
  RETURN ROUND(100 * (choice_sums / (choice_sums + other_sums)), 2);
END$$


CREATE PROCEDURE KPIReport( IN nmgkUnit INT
                          , IN startDate DATE
                          , IN finishDate DATE
                          , IN regionId INT
                          , IN retailerId INT
                          , IN userId INT
                          , IN oosCategories VARCHAR(3255)
                          , IN ssCategories VARCHAR(3255)
                          , OUT sequal VARCHAR(204096))
  BEGIN
    DECLARE sqloos,sqlss VARCHAR(53072);
    DECLARE t1, t2, t3 VARCHAR(60000);
    DECLARE leftAll, middleAll, rightAll VARCHAR(80000);
    DECLARE groupbyClause VARCHAR(50) DEFAULT 'GROUP BY timestamp, userId, taskId, pointId';
    DECLARE whereClause VARCHAR(5255) DEFAULT 'WHERE ';
    DECLARE oosWhereClause, ssWhereClause VARCHAR(6300);
    DECLARE sqloosColumnName VARCHAR(5000);
    DECLARE sqlssColumnName VARCHAR(5000);


    IF (nmgkUnit IS NOT NULL) THEN
      SET whereClause = CONCAT(whereClause, 'nmgkUnit = ', nmgkUnit, ' AND ');
    END IF;

    SET whereClause = CONCAT(whereClause, '(timestamp BETWEEN ''', startDate, ''' AND ''', finishDate, ''' )');

    IF (regionId IS NOT NULL) THEN
      SET whereClause = CONCAT(whereClause, ' AND regionId = ', regionId);
    END IF;

    IF (retailerId IS NOT NULL) THEN
      SET whereClause = CONCAT(whereClause, ' AND retailerId = ', retailerId);
    END IF;

    IF (userId IS NOT NULL) THEN
      SET whereClause = CONCAT(whereClause, ' AND userId = ', userId);
    END IF;

    IF (oosCategories IS NOT NULL) THEN
        SELECT GROUP_CONCAT(DISTINCT(CONCAT('OutOfStock(SUM(IF(oos.categoryId = ''', categoryId, ''', oos.value, NULL)), COUNT(IF(oos.categoryId = ''', categoryId, ''', oos.value, NULL))) AS oos_cid_', categoryId))) INTO sqloos FROM oos WHERE (timestamp BETWEEN startDate AND finishDate) AND FIND_IN_SET(categoryId, oosCategories);
        
        SELECT GROUP_CONCAT(DISTINCT(CONCAT('oos_cid_', categoryId))) INTO sqloosColumnName FROM oos WHERE (timestamp BETWEEN startDate AND finishDate) AND FIND_IN_SET(categoryId, oosCategories);

        SET oosWhereClause = CONCAT(whereClause, ' AND FIND_IN_SET(oos.categoryId,''', oosCategories, ''')');
       
      ELSE SELECT GROUP_CONCAT(DISTINCT(CONCAT('OutOfStock(SUM(IF(oos.categoryId = ''', categoryId, ''', oos.value, NULL)), COUNT(IF(oos.categoryId = ''', categoryId, ''', oos.value, NULL))) AS oos_cid_', categoryId))) INTO sqloos FROM oos WHERE timestamp BETWEEN startDate AND finishDate;

        SELECT GROUP_CONCAT(DISTINCT(CONCAT('oos_cid_', categoryId))) INTO sqloosColumnName FROM oos WHERE timestamp BETWEEN startDate AND finishDate;

        SET oosWhereClause = whereClause;
    END IF;

    IF (ssCategories IS NOT NULL) THEN
       SELECT GROUP_CONCAT(DISTINCT(CONCAT('ShelfShare(SUM(IF(shelfshare.categoryId = ''', categoryId, ''', IF(shelfshare.manufacturerId = ''', 75, ''', shelfshare.value, 0), NULL)), SUM(IF(shelfshare.categoryId = ''', categoryId, ''', IF(shelfshare.manufacturerId <> ''', 75, ''', shelfshare.value, 0), NULL))) AS ss_cid_', categoryId))) INTO sqlss FROM shelfshare WHERE (timestamp BETWEEN startDate AND finishDate) AND FIND_IN_SET(categoryId, ssCategories);

       SELECT GROUP_CONCAT(DISTINCT(CONCAT('ss_cid_', categoryId))) INTO sqlssColumnName FROM shelfshare WHERE (timestamp BETWEEN startDate AND finishDate) AND FIND_IN_SET(categoryId, ssCategories);

       SET ssWhereClause = CONCAT(whereClause, ' AND FIND_IN_SET(shelfshare.categoryId, ''', ssCategories, ''')');
      
    ELSE SELECT GROUP_CONCAT(DISTINCT(CONCAT('ShelfShare(SUM(IF(shelfshare.categoryId = ''', categoryId, ''', IF(shelfshare.manufacturerId = ''', 75, ''', shelfshare.value, 0), NULL)), SUM(IF(shelfshare.categoryId = ''', categoryId, ''', IF(shelfshare.manufacturerId <> ''', 75, ''', shelfshare.value, 0), NULL))) AS ss_cid_', categoryId))) INTO sqlss FROM shelfshare WHERE (timestamp BETWEEN startDate AND finishDate);

       SELECT GROUP_CONCAT(DISTINCT(CONCAT('ss_cid_', categoryId))) INTO sqlssColumnName FROM shelfshare WHERE (timestamp BETWEEN startDate AND finishDate);

       SET ssWhereClause = whereClause;
    END IF;

    SET t1 = CONCAT('(SELECT userId, timestamp, taskId, nmgkUnit, regionId, cityId, pointId, retailerId, OutOfStock(SUM(oos.value), COUNT(oos.value)) as oos_nmgk_total, ', sqloos, ' FROM oos ', oosWhereClause, ' ', groupbyClause,') AS T1');

    SET t2 = CONCAT('(SELECT userId, timestamp, taskId, nmgkUnit, regionId, cityId, pointId, retailerId, ShelfShare(SUM(IF(shelfshare.manufacturerId = 75, shelfshare.value, NULL)), SUM(IF(shelfshare.manufacturerId <> 75, shelfshare.value, NULL))) as ss_nmgk_total, ', sqlss, ' FROM shelfshare ', ssWhereClause, ' ', groupbyClause,') AS T2');

    SET t3 = CONCAT('(SELECT userId, timestamp, taskId, nmgkUnit, regionId, cityId, pointId, retailerId, GROUP_CONCAT(IF(abphoto.type = ''', "photo_before", ''', abphoto.value, NULL)) AS photo_before, GROUP_CONCAT(IF(abphoto.type = ''', "photo_after", ''', abphoto.value,NULL)) AS photo_after FROM abphoto ', whereClause,' ', groupbyClause,') AS T3');

    SET leftAll = CONCAT('SELECT T1.userId,T1.timestamp,T1.taskId,T1.nmgkUnit,T1.regionId,T1.cityId,T1.pointId,T1.retailerId,T1.oos_nmgk_total,', sqloosColumnName, ',', sqlssColumnName,',T3.photo_before, T3.photo_after FROM (', t1, ' LEFT OUTER JOIN ', t2, ' ON (T1.taskId = T2.taskId) LEFT OUTER JOIN ', t3, ' ON (T1.taskId = T3.taskId))');
    SET middleAll = CONCAT('SELECT T2.userId,T2.timestamp,T2.taskId,T2.nmgkUnit,T2.regionId,T2.cityId,T2.pointId,T2.retailerId,T1.oos_nmgk_total,', sqloosColumnName, ',', sqlssColumnName,',T3.photo_before, T3.photo_after FROM (', t1, ' RIGHT OUTER JOIN ', t2, ' ON (T2.taskId = T1.taskId) LEFT OUTER JOIN ', t3, ' ON (T2.taskId = T3.taskId))');
    SET rightAll = CONCAT('SELECT T3.userId,T3.timestamp,T3.taskId,T3.nmgkUnit,T3.regionId,T3.cityId,T3.pointId,T3.retailerId,T1.oos_nmgk_total,', sqloosColumnName, ',', sqlssColumnName,',T3.photo_before, T3.photo_after FROM (', t1, ' RIGHT OUTER JOIN ', t2, ' ON (T2.taskId = T1.taskId) RIGHT OUTER JOIN ', t3, ' ON (T3.taskId = T2.taskId))');

    SET sequal = CONCAT(leftALL, ' UNION ', middleALL, ' UNION ', rightALL, ';');

  END$$

DELIMITER ;
 
